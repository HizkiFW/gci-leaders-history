# GCI Leaders Archive

Welcome! This repo will contain hourly archives of the 2017 GCI Leaders list, fetched from https://gci-leaders.netlify.com/data.json

The archived data can be seen on the [data branch](https://gitlab.com/HizkiFW/gci-leaders-history/tree/data/data).